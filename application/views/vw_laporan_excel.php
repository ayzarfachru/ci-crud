 <?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table width="100%">
 
      <thead>
 
           <tr>
 
                <th>Nim</th>
 
                <th>Nama</th>
 
                <th>Alamat</th>
 
           </tr>
 
      </thead>
 
      <tbody>
 
           <?php $i=1; foreach($data as $d) { ?>
 
           <tr>
 
                <td><?php echo $d->nim; ?></td>
                <td><?php echo $d->nama; ?></td>
                <td><?php echo $d->alamat; ?></td>
 
           </tr>
 
           <?php $i++; } ?>
 
      </tbody>
 
 </table>