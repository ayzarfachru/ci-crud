<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crudmodel extends CI_Model {

	public function Getmahasiswa($where = ""){
		$data = $this->db->query('select * from mahasiswa '. $where);
		return $data->result_array();
	}

	public function InsertData($tabelName,$data){
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function UpdateData($tabelName,$data,$where){
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

	public function DeleteData($tabelName,$where){
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	public function ambil_login($username,$password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('login');
		if ($query->num_rows()>0){
			foreach ($query->result() as $row) {
				$sess = array ('username' => $row->username,
					'password' => $row->password
				);
			}
			$this->session->get_userdata($sess);
			redirect('crud/crud_data');
		}else{
			$this->session->set_flashdata('info','Maaf, username atau password anda salah !!!');
			redirect('crud');
		}
	}

	 public function getAll() {
           $this->db->select('*');
           $this->db->from('mahasiswa');
           $query = $this->db->get();
           return $query->result();
      }
}