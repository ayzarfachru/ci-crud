<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function index()
	{
		$this->load->view('Loginview');

	}

	public function ceklogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->load->model('Crudmodel');
		$this->crudmodel->ambil_login($username,$password);
	}

	public function logout()
	{
		$this->session->set_userdata('username', FALSE);
		$this->session->sess_destroy();
		redirect('crud');
	}

	public function crud_data()
	{
		$data = $this->crudmodel->Getmahasiswa();
		$this->load->view('Crudview',array('data' => $data));
	}

	public function regist_data()
	{
		$this->load->view('Registview');
	}

	public function do_regist()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$data_insert = array(
			'username' => $username,
			'password' => $password
		);
		$res = $this->crudmodel->InsertData('login',$data_insert);
		if($res>=1){
			redirect('crud');
		}else{
			"Insert Failed";
		}
	}

	public function add_data()
	{
		$this->load->view('Addview');
	}

	public function do_insert()
	{
		$nim = $_POST['nim'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$data_insert = array(
			'nim' => $nim,
			'nama' => $nama,
			'alamat' => $alamat
		);
		$res = $this->crudmodel->InsertData('mahasiswa',$data_insert);
		if($res>=1){
			redirect('crud/crud_data');
		}else{
			"Insert Failed";
		}
	}

	public function do_delete($nim)
	{
		$where = array('nim' => $nim);
		$res = $this->crudmodel->DeleteData('mahasiswa',$where);
		if($res>=1){
			redirect('crud/crud_data');
		}
	}

	public function edit_data($nim)
	{
		$mhs = $this->crudmodel->GetMahasiswa("where nim = '$nim'");
		$data = array(
			"nim" => $mhs[0]['nim'],
			"nama" => $mhs[0]['nama'],
			"alamat" => $mhs[0]['alamat']
		);
		$this->load->view('Editview',$data);
	}

	public function do_update()
	{
		$nim = $_POST['nim'];
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$data_update = array(
			'nama' => $nama,
			'alamat' => $alamat
		);
		$where = array('nim' => $nim);
		$res = $this->crudmodel->UpdateData('mahasiswa',$data_update,$where);
		if($res>=1){
			redirect('crud/crud_data');
		}else{
			"Insert Failed";
		}
	}

	public function export_excel(){
		$data = array( 'title' => 'Laporan_Data_Mahasiswa',
			'data' => $this->crudmodel->getAll());

		$this->load->view('vw_laporan_excel',$data);
	}
}